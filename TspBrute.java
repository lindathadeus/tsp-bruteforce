import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TspBrute{

	private static int nNodes=5;//no. of cities
	private static double[][] d;//distance matrix
	private static int bestCost=Integer.MAX_VALUE;
	private static ArrayList<Integer> bestPath = new ArrayList<Integer>(nNodes);
	
	private static int iterations;
   
    public static void genPerm(ArrayList<Integer> a) {
        ArrayList<Integer> sub = new ArrayList<Integer>();
        genPerm(sub, a);
    }

 public static void genPerm(ArrayList<Integer> sub, ArrayList<Integer> a) {
	 iterations++;
     int L = a.size();
     if (L == 0){ 
    	 System.out.print(sub);
    	 System.out.println(" Cost: "+getPathCost(sub));
     }
     else {
         
         for (int i = 0; i < L; i++) {
             ArrayList<Integer> ab = new ArrayList<Integer>(sub);
             ab.add(a.get(i));
             ArrayList<Integer> bc = new ArrayList<Integer>(a);
             bc.remove(i);
             genPerm(ab, bc);
         }
     }
 }


    public static void main(String[] args) {
       
    	
    	System.out.println("Brute Force Approach Program \n Author:Linda J");
    	
    	System.out.println("The Program Starts Execution....");
    	
       ArrayList<Integer> nodes = new ArrayList<Integer>(nNodes);
       
       for(int i=0;i<nNodes;i++){
    	   nodes.add(i, i);
       }
    	   
       //load cost matrix
       loadCostMatrix();
       
       //perform brute force cost calculation
       genPerm(nodes);
       
       //best path
       System.out.println("\nBrute-Force Approach" +
       		"\nBest Path: "+bestPath+"\nIts Cost: "+bestCost);
       
      
       System.out.println("Total Iterations: "+getIterations());
       System.out.println("The Program successfully finished its Execution!!!");
    }
    
    public static void loadCostMatrix(){
    	
		 //calculation of distance Matrix 
        d = new double[nNodes][nNodes];
	 //Reading Distance Matrix from the file
        int et_i=0,et_j=0;
        try{
        	File et_file = new File("disMatrix" + nNodes + ".txt");//costs are written in a file
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(et_file));
        	
        	while((each_row=br.readLine()) != null){
        		String et_values[]=each_row.split(","); 
        		List<String> list = Arrays.asList(et_values);
        		Collections.reverse(list);
        		for(et_j=0;et_j<list.size();et_j++)
        			d[et_i][et_j]=Double.parseDouble(list.get(et_j));
        		et_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        System.out.println("Cost Matrix");
        for(int i = 0; i < nNodes; i++){
            for(int j = 0; j < nNodes; j++)
            	System.out.print(" "+d[i][j]);
            System.out.println(" ");
        }
       
                
        System.out.println("\n");

    }
    
    public static int getPathCost(ArrayList<Integer> path){
    	
    	int pathCost=0;
    	
    	for(int i=0;i<path.size()-1;i++)
    		pathCost+=d[i][i+1];
    	pathCost+=d[path.get(path.size()-1)][path.get(0)];
    	
    	
    	//update best path
    	if(bestCost>pathCost){
    		bestCost=pathCost;
    		bestPath=path;
    	}
    	
    	return pathCost;
    }
    
    public static ArrayList<Integer> getBestPath(){
    	return bestPath;
    }
    
    public static int getBestPathCost(){
    	return bestCost;
    }
    
    public static int getIterations(){
    	return iterations;
    }

    
}


